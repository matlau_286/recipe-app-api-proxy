# Recipe App API Proxy

NGINX proxy app for recipe app API

## Usage

## Environment Variables

- `LISTEN_PORT` - Port to listen on (default: `8000`)
- `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
- `APP_PORT` - Port of the app to forward requests to (default: `9000`)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
